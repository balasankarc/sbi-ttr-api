# frozen_string_literal: true

Rails.application.routes.draw do
  get 'currencies' => 'rates#currencies'

  get ':param' => 'rates#index'
  get ':param1/:param2' => 'rates#show'

  match '*unmatched', to: 'application#not_found_method', via: :all
end
