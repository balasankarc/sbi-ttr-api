# frozen_string_literal: true

class ApplicationController < ActionController::API
  def not_found_method
    render json: {
      message: 'Endpoint not supported.'
    }, status: :not_found
  end
end
