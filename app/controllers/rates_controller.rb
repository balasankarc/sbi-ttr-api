# frozen_string_literal: true

class RatesController < ApplicationController
  IncompleteParamsError = Class.new(StandardError)
  MissingDataError = Class.new(StandardError)
  InvalidCurrencyCode = Class.new(StandardError)

  def index
    Date.parse(params[:param])
    date_index
  rescue Date::Error
    currency_index
  end

  def show
    begin
      Date.parse(params[:param1])
      date = params[:param1]
      currency = params[:param2]
    rescue Date::Error
      date = Date.parse(params[:param2])
      currency = params[:param1]
    end

    logger.debug "Getting data for #{currency} on #{date}"

    raise IncompleteParamsError if date.nil? || currency.nil?

    @rate = Rate.find_by(currency:, date:)

    raise MissingDataError unless @rate
  rescue IncompleteParamsError
    render json: {
      message: 'Incomplete parameters. Please specify currency and date.'
    }, status: :internal_server_error
  rescue MissingDataError
    render json: {
      message: 'Rate data missing for specified date and currency.'
    }, status: :not_found
  rescue Date::Error
    render json: {
      message: 'Invalid date. Please specify date in yyyy-mm-dd format.'
    }, status: :internal_server_error
  else
    render json: @rate, except: %i[created_at updated_at]
  end

  def currency_index
    @currencies = Rate.distinct.pluck(:currency)
    raise InvalidCurrencyCode unless @currencies.include?(params[:param])

    @rates = Rate.for(params[:param]).each_with_object({}) do |item, result|
      result[item.date] = item.attributes.except('date', 'currency', 'created_at', 'updated_at')
    end

    raise MissingDataError if @rates.empty?
  rescue InvalidCurrencyCode
    render json: {
      message: 'Invalid parameter.'
    }, status: :internal_server_error
  rescue MissingDataError
    render json: {
      message: 'Rate data missing for specified currency.'
    }, status: :not_found
  else
    render json: @rates, except: %i[created_at updated_at]
  end

  def date_index
    @rates = Rate.on(params[:param]).each_with_object({}) do |item, result|
      result[item.currency] = item.attributes.except('date', 'currency', 'created_at', 'updated_at')
    end

    raise MissingDataError if @rates.empty?
  rescue MissingDataError
    render json: {
      message: 'Rate data missing for specified date.'
    }, status: :not_found
  else
    render json: @rates, except: %i[created_at updated_at]
  end

  def currencies
    @currencies = Rate.distinct.pluck(:currency)
    render json: @currencies
  end
end
