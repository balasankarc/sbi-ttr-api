# frozen_string_literal: true

class Rate < ApplicationRecord
  validates :currency, uniqueness: { scope: :date }

  scope :on, ->(date) { where(date:) }
  scope :for, ->(currency) { where(currency:) }
end
