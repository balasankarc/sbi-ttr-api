# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Rates', type: :request do
  fixtures :rate

  describe 'GET /' do
    context 'with both parameters specified' do
      context 'in /:date/:currency format' do
        context 'with valid values' do
          it 'returns data correctly' do
            get '/2020-07-31/NOK'

            expect(response.parsed_body).to eq(
              {
                'id' => 692_542_166,
                'currency' => 'NOK',
                'date' => '2020-07-31',
                'tt_buy' => '8.17',
                'tt_sell' => '8.36',
                'bill_buy' => '8.15',
                'bill_sell' => '8.37',
                'forex_travel_card_buy' => '0.0',
                'forex_travel_card_sell' => '0.0',
                'cn_buy' => '7.75',
                'cn_sell' => '8.65'
              }
            )
          end
        end

        context 'with invalid date' do
          it 'returns error message' do
            get '/2020-07-/NOK'

            expect(response.status).to eq(500)
            expect(response.parsed_body).to eq(
              {
                'message' => 'Invalid date. Please specify date in yyyy-mm-dd format.'
              }
            )
          end
        end
      end

      context 'in /:currency/:date format' do
        context 'with valid values' do
          it 'returns data correctly' do
            get '/NOK/2020-07-31'

            expect(response.parsed_body).to eq(
              {
                'id' => 692_542_166,
                'currency' => 'NOK',
                'date' => '2020-07-31',
                'tt_buy' => '8.17',
                'tt_sell' => '8.36',
                'bill_buy' => '8.15',
                'bill_sell' => '8.37',
                'forex_travel_card_buy' => '0.0',
                'forex_travel_card_sell' => '0.0',
                'cn_buy' => '7.75',
                'cn_sell' => '8.65'
              }
            )
          end
        end

        context 'with invalid date' do
          it 'returns error message' do
            get '/NOK/2020-07-'

            expect(response.status).to eq(500)
            expect(response.parsed_body).to eq(
              {
                'message' => 'Invalid date. Please specify date in yyyy-mm-dd format.'
              }
            )
          end
        end

        context 'with empty results' do
          it 'returns error message' do
            get '/NOK/2024-12-31'

            expect(response.status).to eq(404)
            expect(response.parsed_body).to eq(
              {
                'message' => 'Rate data missing for specified date and currency.'
              }
            )
          end
        end
      end
    end
  end
end
