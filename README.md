## SBI TTR API

An API to get SBI TTR rates for different currencies on different dates.

Seed data is being fetched from the amazing work Sahil Gupta does at
https://github.com/sahilgupta/sbi-fx-ratekeeper.
