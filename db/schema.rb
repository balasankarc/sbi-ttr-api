# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.1].define(version: 2024_07_04_173732) do
  create_table "rates", force: :cascade do |t|
    t.string "currency", null: false
    t.date "date", null: false
    t.decimal "tt_buy", null: false
    t.decimal "tt_sell", null: false
    t.decimal "bill_buy", null: false
    t.decimal "bill_sell", null: false
    t.decimal "forex_travel_card_buy", null: false
    t.decimal "forex_travel_card_sell", null: false
    t.decimal "cn_buy", null: false
    t.decimal "cn_sell", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["currency", "date"], name: "index_rates_on_currency_and_date", unique: true
    t.index ["currency"], name: "index_rates_on_currency"
    t.index ["date"], name: "index_rates_on_date"
  end

end
