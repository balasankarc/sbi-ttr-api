# frozen_string_literal: true

class CreateRates < ActiveRecord::Migration[7.1]
  def change
    create_table :rates do |t|
      t.string :currency, null: false, index: true
      t.date :date, null: false, index: true
      t.decimal :tt_buy, null: false
      t.decimal :tt_sell, null: false
      t.decimal :bill_buy, null: false
      t.decimal :bill_sell, null: false
      t.decimal :forex_travel_card_buy, null: false
      t.decimal :forex_travel_card_sell, null: false
      t.decimal :cn_buy, null: false
      t.decimal :cn_sell, null: false

      t.index %i[currency date], unique: true

      t.timestamps
    end
  end
end
