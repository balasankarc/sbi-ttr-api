# frozen_string_literal: true

require 'csv'

UPSTREAM_REPO = 'https://github.com/sahilgupta/sbi-fx-ratekeeper.git'

if Dir.exist?('/tmp/ttr-upstream')
  `cd /tmp/ttr-upstream && git pull origin`
else
  `git clone --depth=1 #{UPSTREAM_REPO} /tmp/ttr-upstream`
end

csv_files = Dir.glob('/tmp/ttr-upstream/csv_files/*.csv')

csv_files.each do |file|
  currency = file.match(/SBI_REFERENCE_RATES_(?<currency>.*).csv/)[:currency]

  Rails.logger.debug { "Loading data for #{currency}" }

  csv_data = CSV.parse(File.read(file), headers: true)

  csv_data.each do |line|
    args = {
      currency:,
      date: line['DATE'].split[0].strip,
      tt_buy: line['TT BUY'],
      tt_sell: line['TT SELL'],
      bill_buy: line['BILL BUY'],
      bill_sell: line['BILL SELL'],
      forex_travel_card_buy: line['FOREX TRAVEL CARD BUY'],
      forex_travel_card_sell: line['FOREX TRAVEL CARD SELL'],
      cn_buy: line['CN BUY'],
      cn_sell: line['CN SELL']
    }

    next unless args.values.all?

    r = Rate.new(args)
    r.save
  end
end
